# -*- encoding: utf-8 -*-
import os

import socket


def http_serve(server_socket):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                print "Odebrano:"
                print request
                address = (request.split("\n")[0])[5:-10]
                print address
                if address=="":
                    print os.listdir(".")
                    html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                    html += "<!DOCTYPE html><html><head></head><body>"
                    items = os.listdir(".")
                    for item in items:
                        print address+item
                        if not "." in item:
                            item+="/"
                        html+= "<a href=\""+item+"\">"+item+"</a><br />"
                    html += "</body></html>"
                elif address[-4:]=="html":
                    try:
                        html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+= open(address,"rb").read()
                    except:
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                elif address[-3:]=="txt" or address[-2:]=="py" or address[-3:]=="rst":
                    try:
                        html = "HTTP/1.1 200 OK Content-Type: text/span; charset=UTF-8\r\n\r\n"
                        html += open(address,"rb").read()
                    except:
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                elif address[-3:]=="png":
                    try:
                        html = "HTTP/1.0 200 OK Content-Type: image/png;\r\n\r\n"
                        html += open(address,"rb").read()

                    except :
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                elif address[-3:] == "jpg":
                    try:
                        print address
                        html = "HTTP/1.1 200 OK Content-Type: image/jpeg; charset=UTF-8\r\n\r\n"
                        html+= open(address,"rb").read()
                    except:
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                elif not "." in address:
                    print address
                    try:
                        print os.listdir(address)
                        html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html += "<!DOCTYPE html><html><head></head><body>"
                        items = os.listdir(address)
                        for item in items:
                            print address+item
                            if "." not in item:
                                item+="/"
                            html+= "<a href=\""+item+"\">"+item+"</a><br />"
                        html += "</body></html>"
                    except:
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                else:
                    try:
                        html = "HTTP/1.1 200 OK Content-Type: plain/text; charset=UTF-8\r\n\r\n"
                        html+= open(address).read()
                    except:
                        html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                        html+="<h1>404 not found</h1>"
                        # Wysłanie zawartości strony
                connection.sendall(html)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31002)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

try:
    http_serve(server_socket)

finally:
    server_socket.close()